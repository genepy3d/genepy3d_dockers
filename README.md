# genepy3d_dockers

Dockerfiles with ready to use environements for [GeNePy3d](https://genepy3d.gitlab.io). Pushed to https://hub.docker.com/repository/docker/ac744/genepy3d.
Two containers are available, one with everything and one with only the non-GPL bits. They are based on the scipy_notebook image from the jupyter project.

## Example:
Running from your work directory
>  docker run -it -p 10000:8888 -v "$PWD":/home/jovyan/work ac744/genepy3d:full 

will (download if needed and) run a docker image with genepy and all it's dependency including CGAL and cgal_swig_bindings. Just copy paste the link that will appear on the terminal to you browser, changing port 8888 to 10000 and you are good to go! The change made to files in your work directory will remain after your close the container.




Full information and documatation available at https://genepy3d.gitlab.io